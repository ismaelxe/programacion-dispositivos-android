package com.example.indio.pruebaintents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

public class RecibeSolo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibe_solo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);  // SI queremos aparezca flecha navegación
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);    //... Mirar también en el manifest android:parentActivityName

        Toast.makeText(this, "onCreate RecibeSolo", Toast.LENGTH_SHORT).show();


        TextView mensaje=(TextView)findViewById(R.id.texto);  //Esta activity no retorna nada
        Intent intent = getIntent();
        String cad = intent.getStringExtra("clave");
        mensaje.setText(cad);
    }

    @Override protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart RecibeSolo", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume RecibeSolo", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onPause() {
        Toast.makeText(this, "onPause RecibeSolo", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    @Override protected void onStop() {
        Toast.makeText(this, "onStop RecibeSolo", Toast.LENGTH_SHORT).show();
        super.onStop();
    }

    @Override protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "onRestart RecibeSolo", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
        Toast.makeText(this, "onDestroy RecibeSolo", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

}
