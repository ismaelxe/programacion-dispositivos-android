package com.example.indio.pruebaintents;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static final int CLAVE_DE_RETORNOI = 101;
    static final int CLAVE_DE_RETORNOII =102;
    static final int CLAVE_DE_RETORNOIII =103;

    EditText texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "onCreate PruebaIntents", Toast.LENGTH_SHORT).show();

        Button boton=(Button)findViewById(R.id.button);
        Button boton1=(Button)findViewById(R.id.button1);
        Button boton2=(Button)findViewById(R.id.button2);
        Button boton3=(Button)findViewById(R.id.button3);
        Button boton4=(Button)findViewById(R.id.button4);
        Button boton5=(Button)findViewById(R.id.button5);
        Button boton6=(Button)findViewById(R.id.button6);
        Button boton7=(Button)findViewById(R.id.button7);
        texto=(EditText)findViewById(R.id.text1);

        boton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                Uri webpage = Uri.parse("http://www.android.com");
                Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
                startActivity(webIntent);
            }

    });
        boton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                Uri number = Uri.parse("tel:5551234");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);
            }

        });

        boton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                // Build the intent
                Uri location = Uri.parse("geo:38.9739,-1.85366?z=14"); // z param is zoom level

                Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);

                // Verify it resolves
                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(mapIntent, 0);
                boolean isIntentSafe = activities.size() > 0;

                // Start an activity if it's safe
                if (isIntentSafe) {
                    startActivity(mapIntent);
                }


            }

        });

        boton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                //emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);   //cannot resolve symbol error??
                emailIntent.setType("text/plain");
                Intent intent = emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"jon@example.com"});// recipients
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text");
                emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));

                // Always use string resources for UI text.
                // This says something like "Share this photo with"
                String title = getResources().getString(R.string.chooser_title);
                // Create intent to show chooser
                Intent chooser = Intent.createChooser(emailIntent, title);

                // Verify the intent will resolve to at least one activity
                if (emailIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
            }

        });

        boton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                Intent miIntent = new Intent(Intent.ACTION_SEND);
                miIntent.setType("text/plain");
                miIntent.putExtra("clave", texto.getText().toString());
                startActivityForResult(miIntent, CLAVE_DE_RETORNOI );
            }

        });

         /* para que esta última llamada funcione hay que poner en la activity llamada un intent-filter en el manifiesto

            <intent-filter>
                <action android:name="android.intent.action.SEND"/>
                <category android:name="android.intent.category.DEFAULT"/>
                <data android:mimeType="text/plain"/>
            </intent-filter>

          */

        boton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                Intent miIntent = new Intent("com.example.indio.recibesend.intent.action.RecibeSEND");
                miIntent.setType("text/plain");
                miIntent.putExtra("clave", texto.getText().toString());
                startActivityForResult(miIntent, CLAVE_DE_RETORNOII );
            }

        });

         /* para que esta última llamada funcione hay que poner en la activity llamada un filtro acction en el manifiesto
            el name suele ser  <nombredepaquete>.intent.acction.NOMBREACCION ???

              <intent-filter>
                 <action android:name="com.example.indio.recibesend.intent.action.RecibeSEND" />
                <category android:name="android.intent.category.DEFAULT" />
                <data android:mimeType="text/plain"/>
            </intent-filter>

          */
             //llamadas a activities pertenecientes al mismo proyecto
             // en la primera se usa navegación y no hay retorno
              // en la segunda hay retorno de valor

        boton6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                //Intent miIntent = new Intent(getApplicationContext(),RecibeIntent.class);
                Intent miIntent = new Intent(getApplicationContext(),RecibeSolo.class);
                miIntent.putExtra("clave", texto.getText().toString());
                startActivity(miIntent);
            }

        });
        boton7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0)
            {
                //Intent miIntent = new Intent(getApplicationContext(),RecibeIntent.class);
                Intent miIntent = new Intent(getApplicationContext(),RecibeIntent.class);
                miIntent.putExtra("clave", texto.getText().toString());
                startActivityForResult(miIntent, CLAVE_DE_RETORNOIII );
            }

        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CLAVE_DE_RETORNOI) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
             String cad=data.getDataString();
             texto.setText(cad);

            }
        } else
        if (requestCode == CLAVE_DE_RETORNOII) {         // en este caso hacemos lo mismo pero se muestra por separado
            // Make sure the request was successful      //ya que viene distintas llamadas y lo normal es hacer algo distinto
            if (resultCode == RESULT_OK) {
                String cad=data.getDataString();
                texto.setText(cad);

            }
        }
        if (requestCode == CLAVE_DE_RETORNOIII) {         // en este caso hacemos CASI lo mismo pero se muestra por separado
            // Make sure the request was successful        //ya que viene de activitis distintas y lo normal es hacer algo distinto
            if (resultCode == RESULT_OK) {
                String cad=data.getStringExtra("clave");
                texto.setText(cad);

            }
        }

    }


    @Override protected void onStart() {
        super.onStart();
        Toast.makeText(this, "onStart PruebaIntents", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume PruebaIntents", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onPause() {
        Toast.makeText(this, "onPause PruebaIntents", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    @Override protected void onStop() {
        Toast.makeText(this, "onStop PruebaIntents", Toast.LENGTH_SHORT).show();
        super.onStop();
    }

    @Override protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, "onRestart PruebaIntents", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
        Toast.makeText(this, "onDestroy PruebaIntents", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putString("aSalvar", "Valor a Salvar");

        Toast.makeText(this, "onSaveInstanceState PruebaIntents", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onRestoreInstanceState(Bundle inState){   //el mismo código se puede usar en el onCraate
        String salvado;
        super.onRestoreInstanceState(inState);
        if(inState != null)  salvado=inState.getString("aSalvar");
        else salvado="NO salvado";
        Toast.makeText(this, "onRestoredInstanceState PruebaIntents "+salvado, Toast.LENGTH_SHORT).show();
    }

}