package com.example.indio.pruebaintents;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RecibeIntent extends AppCompatActivity {
    EditText tex;
    Button boton;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibe_intent);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        tex=(EditText)findViewById(R.id.texto);
        //Button=(Button)findViewById()

        Toast.makeText(this, "onCreate RecibeIntent", Toast.LENGTH_SHORT).show();

        Intent intent = getIntent();
        String cad = intent.getStringExtra("clave");
        tex.setText(cad);
    }

    public void retornarValor(View view){
        String cad=tex.getText().toString();
        StringBuilder builder=new StringBuilder(cad);
        cad=builder.reverse().toString();
        Intent datosVuelta = new Intent();
        datosVuelta.putExtra("clave",cad);
        setResult(Activity.RESULT_OK, datosVuelta);
        finish();
    }

    @Override protected void onStart() {
                super.onStart();
                Toast.makeText(this, "onStart  RecibeIntent", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onResume() {
                super.onResume();
                Toast.makeText(this, "onResume RecibeIntent", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onPause() {
                Toast.makeText(this, "onPause RecibeIntent", Toast.LENGTH_SHORT).show();
                super.onPause();
    }

    @Override protected void onStop() {
                Toast.makeText(this, "onStop RecibeIntent", Toast.LENGTH_SHORT).show();
                super.onStop();
    }

    @Override protected void onRestart() {
                super.onRestart();
                Toast.makeText(this, "onRestart RecibeIntent", Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
                Toast.makeText(this, "onDestroy RecibeIntent", Toast.LENGTH_SHORT).show();
                super.onDestroy();
    }




}
