package com.example.ismael.blogdecoches;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ismael.blogdecoches.Adaptadores.AdaptadorCardView;
import com.example.ismael.blogdecoches.Entidades.Articulo;
import com.example.ismael.blogdecoches.Modelo.DBArticulos;

import java.util.ArrayList;

public class ListadoArticulosInicio extends Fragment {



    public ListadoArticulosInicio(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);






    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_listado_articulos_inicio, container, false);


        DBArticulos db;

        ArrayList<Articulo> articulos;
        RecyclerView recyclerView;
        AdaptadorCardView adapter;

        db = new DBArticulos(getActivity());
        articulos = db.getArticulos();


        recyclerView = (RecyclerView)view.findViewById(R.id.rvArticulosInicio);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdaptadorCardView(getActivity(), articulos);


        recyclerView.setAdapter(adapter);



        return view;







    }
}
