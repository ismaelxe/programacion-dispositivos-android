package com.example.ismael.blogdecoches.Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    private final String BDCREATE = "CREATE TABLE ARTICULOS (_id INTEGER PRIMARY KEY AUTOINCREMENT, TITULO TEXT NOT NULL, " +
            "DESCRIPCION TEXT, IMAGENURL TEXT, ARTICULOURL TEXT NOT NULL, CATEGORIA TEXT)";
    private static final int DATABASE_VERSION=3;
    private static final String DATABASE_NAME="ARTICULOS";

    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Ejecutamos la instruccion para crear la base de datos
        sqLiteDatabase.execSQL(BDCREATE);
        insertData(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //Eliminamos la tabla antigua (en realidad habria que migrarla)
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS ARTICULOS");

        //Creamos la nueva version de la bdd
        sqLiteDatabase.execSQL(BDCREATE);

        insertData(sqLiteDatabase);

    }

    private void insertData(SQLiteDatabase sqLiteDatabase){
        String INSERTARDATOS = "INSERT INTO ARTICULOS (TITULO, DESCRIPCION, IMAGENURL, ARTICULOURL, CATEGORIA)" +
                "VALUES  ('Ford Focus', 'Con cuatro motores', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/ford_focus_sportbreak.jpeg?alt=media&token=985068a7-8a8d-40a7-ab68-be4b8418cc80','https://www.coches.net/nuevo-ford-focus-sportbreak-active', 'coches')," +
                "('Range Rover Evoque', 'Mas diseño y con version hibrida', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/land-rover-range-rover-evoque.jpg?alt=media&token=2c2058f5-2586-4584-98db-673d290e535f','https://www.coches.net/nuevo-land-rover-range_rover_evoque-2019', 'coches')," +
                "('Cupra  Ateca', 'Con 350 cv', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/cupra-ateca.jpeg?alt=media&token=3e29e79a-0d5a-40d8-9091-a20c183df132','https://www.coches.net/noticias/abt-cupra-ateca', 'coches')," +
                "('Audi R8: Mas potente y rapido', 'Llega con cambios mecánicos y estéticos', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/audi-r8.jpg?alt=media&token=39cc1fa1-98d9-415b-82b3-c2bd009a08a7','https://www.coches.net/nuevo-audi-r8-2019', 'coches')," +
                "('Honda Monkey 125', 'El mito vuelve mas fuerte que nunca', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/Honda_Monkey_125_2018.jpg?alt=media&token=ec97ed3b-b80a-42b1-a049-17e9faf47e78', 'https://motos.coches.net/noticias/honda-monkey-125-prueba-2018', 'motos')," +
                "('KTM 690 smc R/Enduro', 'Con electronica avanzada y nuevo subchasis para 2019', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/ktm-690.jpg?alt=media&token=bc6ece0c-b46e-466c-a742-9b77083c7629', 'https://motos.coches.net/noticias/ktm-690-smc-r-enduro-r-2019', 'motos')," +
                "('Hyundai Elantra', 'Un sedan compacto y practico', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/2002-hyundai-elantra.jpg?alt=media&token=717cb639-5901-4835-9789-0c179ff9aeba', 'https://www.diariomotor.com/coche/hyundai-elantra/', 'coches')," +
                "('MV Agusta Superveloce 800', 'Aun en fase de preproduccion: Invita a soñar despierto', 'https://firebasestorage.googleapis.com/v0/b/proyecto-android-clase.appspot.com/o/mv-agusta-superveloce-800.jpg?alt=media&token=552caedb-fe07-4b82-a0b0-7112d5144f37', 'https://motos.coches.net/noticias/mv-agusta-superveloce-800-novedad-2019', 'motos')";
        sqLiteDatabase.execSQL(INSERTARDATOS);
//('titulo', 'desc', 'imagen', 'articulo', 'cat')
    }
}
