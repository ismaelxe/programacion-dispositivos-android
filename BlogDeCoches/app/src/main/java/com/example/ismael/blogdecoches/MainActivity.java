package com.example.ismael.blogdecoches;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ismael.blogdecoches.Adaptadores.AdaptadorCardView;
import com.example.ismael.blogdecoches.Entidades.Articulo;
import com.example.ismael.blogdecoches.Modelo.DBArticulos;
import com.example.ismael.blogdecoches.Modelo.DBHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences preferences;
    private ArrayList<Articulo> articulos;
    private RecyclerView recyclerView;
    private AdaptadorCardView adapter;
    private AdView mAdView;
    private static final String TAG = "MainActivity";
    private  NavigationView navigationView;
    private  TextView navHeaderTitle;
    private TextView navHeaderSubtitle;
    private DBArticulos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_datos, new ListadoArticulosInicio())
                .commit();


        inicializarVariables();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        db = new DBArticulos(this);
        articulos = db.getArticulos();

        iniciarNavigationView();

        //iniciarRecyclerView();


        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);







        db.close();

        /////////////////////////////////////////////////// Lenar array para pruebas
        /*
        Articulo articulo;
        for(int i = 0; i < 5; i++){
            articulo = new Articulo();
            articulo.setTitulo("Titulo " + i);
            articulo.setDescripcion("Descripcion " + i);
            //articulos.add(articulo);
        }
*/


        ///////////////////////////////////////////////////





    }

    @Override
    protected void onResume() {
        super.onResume();
        preferences = getSharedPreferences("preferencias",Context.MODE_PRIVATE);
        navHeaderTitle.setText(preferences.getString("nombreUsuario",getResources().getString(R.string.nav_header_title)));
        navHeaderSubtitle.setText(preferences.getString("emailUsuario",getResources().getString(R.string.nav_header_subtitle)));
    }

    private void iniciarRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AdaptadorCardView(this, articulos);



        recyclerView.setAdapter(adapter);
    }

    private void iniciarNavigationView() {
        //Crear NavigationDrawer
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Cargar datos de las preferencias en el drawer
        View headerView = navigationView.getHeaderView(0);
        navHeaderTitle = (TextView)headerView.findViewById(R.id.navHeaderTitle);
        navHeaderSubtitle = (TextView)headerView.findViewById(R.id.navHeaderSubtitle);


        //Cargar NavigationDrawer
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void inicializarVariables() {
        articulos = new ArrayList<>();
        //recyclerView = (RecyclerView)findViewById(R.id.rvArticulosMain);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean fragmentTransaction = false;
        Fragment fragment = null;

        switch (id){
            case R.id.navInicio:
                Toast.makeText(this, "Inicio", Toast.LENGTH_SHORT).show();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_datos, new ListadoArticulosInicio())
                        .commit();
                break;
            case R.id.navBuscador:
                Toast.makeText(this, "Buscador", Toast.LENGTH_SHORT).show();
                break;
            case R.id.navCoches:
                Toast.makeText(this, "Coches", Toast.LENGTH_SHORT).show();
                break;
            case R.id.navMotos:
                Toast.makeText(this, "Motos", Toast.LENGTH_SHORT).show();
                break;

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
