package com.example.ismael.blogdecoches.Modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ismael.blogdecoches.Entidades.Articulo;

import java.util.ArrayList;

public class DBArticulos {
    private SQLiteDatabase db = null;
    private DBHelper dbHelper = null;
    Context context;

    public DBArticulos(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context, null);
        db = dbHelper.getWritableDatabase();

    }

    public void close() {
        dbHelper.close();
    }

    public Articulo getArticuloFromId(int id) {
        Cursor c = db.query("ARTICULOS", new String[]{"_id", "TITULO", "DESCRIPCION", "IMAGENURL", "ARTICULOURL", "CATEGORIA"},
                "_id=?", new String[]{String.valueOf(id)}, null, null, null);
        if (c.moveToNext()) {
            return makeCursorToArticulo(c);
        }
        return null;
    }

    public ArrayList<Articulo> getArticulos() {
        ArrayList<Articulo> articulos = new ArrayList<>();
        Cursor c = db.query("ARTICULOS", new String[]{"_id", "TITULO", "DESCRIPCION", "IMAGENURL", "ARTICULOURL", "CATEGORIA"},
                null, null, null, null, null, "10");
        Articulo articulo;
        while (c.moveToNext()) {
            articulo = makeCursorToArticulo(c);
            articulos.add(articulo);
            Log.d("makeCursor", "getArticulosWhile");
        }
        Log.d("makeCursor", "getArticulos");
        return articulos;
    }

    public ArrayList<Articulo> getArticulosByCategoria(String cat){
        ArrayList<Articulo> articulos = new ArrayList<>();
        Cursor c = db.query("ARTICULOS", new String[]{"_id", "TITULO", "DESCRIPCION", "IMAGENURL", "ARTICULOURL", "CATEGORIA"}, "CATEGORIA",
                new String[]{cat},null,null, null,null);

        Articulo articulo;
        while (c.moveToNext()) {
            articulo = makeCursorToArticulo(c);
            articulos.add(articulo);
            Log.d("makeCursor", "getArticulosWhile");
        }
        Log.d("makeCursor", "getArticulos");
        return articulos;

    }

    private Articulo makeCursorToArticulo(Cursor c) {
        Articulo articulo = new Articulo();

        int idColumn = c.getColumnIndex("_id");
        int tituloColumn = c.getColumnIndex("TITULO");
        int descColumn = c.getColumnIndex("DESCRIPCION");
        int imagenColumn = c.getColumnIndex("IMAGENURL");
        int articuloColumn = c.getColumnIndex("ARTICULOURL");
        articulo.setId(Integer.parseInt(c.getString(idColumn)));
        articulo.setTitulo(c.getString(tituloColumn));
        articulo.setDescripcion(c.getString(descColumn));
        articulo.setImagenURL(c.getString(imagenColumn));
        articulo.setArticuloURL(c.getString(articuloColumn));
        Log.d("makeCursor", articulo.getId() + "");

        return articulo;
    }

}
