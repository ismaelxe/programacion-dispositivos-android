package com.example.ismael.blogdecoches.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ismael.blogdecoches.Entidades.Articulo;
import com.example.ismael.blogdecoches.MainActivity;
import com.example.ismael.blogdecoches.R;
import com.example.ismael.blogdecoches.WebViewActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorCardView extends RecyclerView.Adapter<AdaptadorCardView.ViewHolder>{

    private ArrayList<Articulo> listDatos;
    private Context context;

    public AdaptadorCardView(Context context, ArrayList<Articulo> list){
        this.context = context;
        listDatos = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_main,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {
        holder.titulo.setText(listDatos.get(i).getTitulo());
        holder.descripcion.setText(listDatos.get(i).getDescripcion());
        Picasso.with(context).load(listDatos.get(i).getImagenURL()).into(holder.imagen);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("articulo", listDatos.get(i));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listDatos.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView titulo, descripcion;
        ImageView imagen;
        CardView cardView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            titulo = (TextView)itemView.findViewById(R.id.tvTituloItemMain);
            descripcion = (TextView)itemView.findViewById(R.id.tvDescripcionItemMain);
            imagen = (ImageView)itemView.findViewById(R.id.ivItemMain);
            cardView = (CardView)itemView.findViewById(R.id.cwItemMain);



        }
    }
}
