package com.example.ismael.blogdecoches;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.ismael.blogdecoches.Entidades.Articulo;

public class WebViewActivity extends AppCompatActivity {
    private Intent intent;
    private WebView webView;
    private Articulo articulo;
    private WebSettings webSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);


        webView = (WebView)findViewById(R.id.wvMostrarArticulo);
        intent = this.getIntent();
        articulo = (Articulo) intent.getSerializableExtra("articulo");
        Log.d("url articulo", articulo.getArticuloURL());
        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(false);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(articulo.getArticuloURL());


    }
}
