package com.example.ismael.blogdecoches.Entidades;

import android.media.Image;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Articulo implements Serializable {
    private int id;
    private String titulo, descripcion;
    private String imagenURL;
    private String articuloURL;
    private String categoria;

    public Articulo(){}

    public Articulo(int id, String titulo, String descripcion, String imagenURL, String articuloURL, Date fecha){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagenURL() {
        return imagenURL;
    }

    public void setImagenURL(String imagenURL) {
        this.imagenURL = imagenURL;
    }

    public String getArticuloURL() {
        return articuloURL;
    }

    public void setArticuloURL(String articuloURL) {
        this.articuloURL = articuloURL;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

}
