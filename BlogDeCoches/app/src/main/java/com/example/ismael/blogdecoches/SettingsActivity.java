package com.example.ismael.blogdecoches;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Spinner spIdioma;
    private EditText etNombre, etEmail;
    private ArrayList<String> idiomas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        sharedPreferences = getSharedPreferences("preferencias",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        idiomas = new ArrayList();
        idiomas.add("español");
        idiomas.add("ingles");

        spIdioma = (Spinner)findViewById(R.id.spSeleccionarIdioma);
        etNombre = findViewById(R.id.etNombreUsuario);
        etEmail = findViewById(R.id.etEmailUsuario);

        cargarPreferencias();

        spIdioma.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,idiomas));
    }

    private void cargarPreferencias() {
        etNombre.setText(sharedPreferences.getString("nombreUsuario", ""));
        etEmail.setText(sharedPreferences.getString("emailUsuario", ""));
    }


    @Override
    protected void onPause() {
        super.onPause();

        editor.putString("nombreUsuario", etNombre.getText().toString());
        editor.putString("emailUsuario", etEmail.getText().toString());
        editor.putString("idioma", spIdioma.getSelectedItem().toString());
        editor.commit();

    }
}
