package com.miguel.vinosvillamalea;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class ActivitySaac extends AppCompatActivity {

    private String linea = "";
    private TextView txtTexto;

    private String saac[]=new String[]{"Altos Blanco","Altos Semidulce","Altos Rosado","Altos Tinto","Flor del Paraiso Blanco","Flor del Paraiso Rosado",
            "Flor del Paraiso Tinto","Gredas Viejas","Kylix Blanco","Kylix Rosado","Kylix Tinto","Providencia Blanco","Providencia Tinto","Viñamalea Tinto"};

    private Integer[] imgid={
            R.drawable.altosdelcabrielblanco,
            R.drawable.altosdelcabrielsemidulceblanco,
            R.drawable.altosdelcabrielrosado,
            R.drawable.altosdelcabrieltinto,
            R.drawable.flordelparaisoblanco,
            R.drawable.flordelparaisorosado,
            R.drawable.flordelparaisotinto,
            R.drawable.gredasviejastinto,
            R.drawable.kylixblanco,
            R.drawable.kylixrosado,
            R.drawable.kylixtinto,
            R.drawable.providenciablanco,
            R.drawable.providenciatinto,
            R.drawable.vinamaleatinto

    };

    private ListView listasaac;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saac);

        SaacListAdapter adapter=new SaacListAdapter(this,saac,imgid);
        listasaac=(ListView)findViewById(R.id.LstSaac);
        listasaac.setAdapter(adapter);
        listasaac.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String Slecteditem= saac[+position];
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();
                Intent lstsaac = new Intent (ActivitySaac.this, Resultado.class);
                //llamada(Slecteditem);
                lstsaac.putExtra("imagen", imgid[position]);
                lstsaac.putExtra("vino", Slecteditem);
                startActivity(lstsaac);

            }
        });

    }
    @SuppressLint("ResourceType")
    public void llamada(String v) {
        switch (v) {

            case ("Altos Blanco"):
                try
                {
                    InputStream fraw =
                            getResources().openRawResource(R.raw.altosblanco);

                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));

                    linea = brin.readLine();
                    fraw.close();


                    txtTexto.setText("Fichero RAW leido!");
                    txtTexto.setText("Texto: " + linea);
                }
                catch (Exception ex)
                {
                    txtTexto.setText( "Error al leer fichero desde recurso raw");;
                }
                break;
            case ("Altos Semidulce"):
                try {
                    InputStream fraw = getResources().openRawResource(R.raw.altossemidulceblanco);
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(fraw));
                    setContentView(R.drawable.altosdelcabrielsemidulceblanco);
                    String texto = fin.readLine();
                    Log.d("ismael ", texto);
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Altos Rosado"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("altosrosado")));
                    setContentView(R.drawable.altosdelcabrielrosado);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Altos Tinto"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("altostinto")));
                    setContentView(R.drawable.altosdelcabrieltinto);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Blanco"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("florblanco")));
                    setContentView(R.drawable.flordelparaisoblanco);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Rosado"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("florrosado")));
                    setContentView(R.drawable.flordelparaisorosado);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Tinto"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("flortinto")));
                    setContentView(R.drawable.flordelparaisotinto);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Gredas Viejas"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("gredastinto")));
                    setContentView(R.drawable.gredasviejastinto);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Kylix Blanco"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("Kylixblanco")));
                    setContentView(R.drawable.kylixblanco);
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;

        /*btnsaac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saac = new Intent(ActivitySaac.this, MainActivity.class);
                startActivity(saac);
            }
        });*/
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}