package com.miguel.vinosvillamalea;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public  class ActivityVitivinos extends AppCompatActivity {
    private Button btnvitivinos;
    private TextView txtTexto;
    private String linea = "";

    private String vitivinos[]=new String[]{"Azua Blanco","Azua Semidulce","Azua Verdejo","Azua Rosado","Azua Crianza","Azua Roble",
            "Azua Reserva","Llanos del Marques"};

    private Integer[] imgid={
            R.drawable.azuablanco,
            R.drawable.azuablancosemidulce,
            R.drawable.azuablancoverdejo,
            R.drawable.azuarosado,
            R.drawable.azuabobalcrianza,
            R.drawable.azuabobalroble,
            R.drawable.azuabobalreserva,
            R.drawable.llanosdelmarquestinto,

    };

    private ListView listavitivinos;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vitivinos);

        VitivinosListAdapter adapter=new VitivinosListAdapter(this,vitivinos,imgid);
        listavitivinos=(ListView)findViewById(R.id.LstVitivinos);
        listavitivinos.setAdapter(adapter);
        listavitivinos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String Slecteditem= vitivinos[+position];
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();
                Intent lstviti = new Intent (ActivityVitivinos.this, Resultado.class);
                startActivity(lstviti);
            }
        });




    }
    @SuppressLint("ResourceType")
        public void onClick(View v) {
            switch (v.getId()) {
                case (R.raw.azuablanco):
                    try
                    {
                        InputStream fraw =
                                getResources().openRawResource(R.raw.azuablanco);

                        BufferedReader brin =
                                new BufferedReader(new InputStreamReader(fraw));

                        linea = brin.readLine();
                        fraw.close();


                        txtTexto.setText("Fichero RAW leido!");
                        txtTexto.setText("Texto: " + linea);
                    }
                    catch (Exception ex)
                    {
                        txtTexto.setText( "Error al leer fichero desde recurso raw");;
                    }

                    break;
                case (R.raw.azuasemidulce):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("azuasemidulce")));
                        setContentView(R.drawable.azuablancosemidulce);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.azuaverdejo):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("azuaverdejo")));
                        setContentView(R.drawable.azuablancoverdejo);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.azuarosado):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("azuarosado")));
                        setContentView(R.drawable.azuarosado);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.azuareserva):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("azuareserva")));
                        setContentView(R.drawable.azuabobalreserva);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.azuacrianza):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("altoscrianza")));
                        setContentView(R.drawable.azuabobalcrianza);

                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.azuaroble):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("azuaroble")));
                        setContentView(R.drawable.azuabobalroble);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
                case (R.raw.llanosdelmarquestinto):
                    try {
                        BufferedReader fin =
                                new BufferedReader(
                                        new InputStreamReader(
                                                openFileInput("llanosdelmarquestinto")));
                        setContentView(R.drawable.llanosdelmarquestinto);
                        String texto = fin.readLine();
                        fin.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                    }

                    break;
            }
    }

    }



