package com.miguel.vinosvillamalea;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.InputStreamReader;



public class MainActivity extends AppCompatActivity {


    Button btnsaac, btnvitivinos;
    ActionBar actionBar;

    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharpref = getSharedPreferences("altosblanco", context.MODE_PRIVATE);
        String valor = sharpref.getString("Dato", "No hay datos");
        Toast.makeText(getApplicationContext(), "Dato Guardado : " + valor, Toast.LENGTH_LONG).show();


        btnsaac = (Button)findViewById(R.id.btnsaac);
        btnvitivinos = (Button)findViewById(R.id.btnvitivinos);


        btnsaac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saac = new Intent(MainActivity.this, ActivitySaac.class);
                startActivity(saac);
            }
        });

        btnvitivinos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vitivinos = new Intent(MainActivity.this, ActivityVitivinos.class);
                startActivity(vitivinos);
            }
        });
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_ab, menu);
        return  true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings){

            return  true;
        }

        return false;
    }

}








