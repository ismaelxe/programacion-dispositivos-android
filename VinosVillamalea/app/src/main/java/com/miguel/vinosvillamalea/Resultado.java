package com.miguel.vinosvillamalea;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Resultado extends AppCompatActivity {
    private ImageView imagen;
    private TextView txtTexto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        imagen = (ImageView)findViewById(R.id.imageView);
        txtTexto = (TextView)findViewById(R.id.textView2);
        Intent intent = getIntent();

        llamada(intent.getStringExtra("vino"));
        imagen.setImageResource(intent.getIntExtra("imagen",0));



    }


    public void llamada(String v) {
        String linea;
        switch (v) {

            case ("Altos Blanco"):
                try
                {
                    txtTexto.setText("Fichero RAW leido!");
                    InputStream fraw =
                            getResources().openRawResource(R.raw.altosblanco);

                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));

                    while((linea = brin.readLine()) != null){
                        txtTexto.setText(txtTexto.getText() + linea);
                    }



                    fraw.close();

                }
                catch (Exception ex)
                {
                    txtTexto.setText( "Error al leer fichero desde recurso raw");;
                }
                break;
            case ("Altos Semidulce"):
                try {
                    InputStream fraw = getResources().openRawResource(R.raw.altossemidulceblanco);
                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));

                    while((linea = brin.readLine()) != null){
                        txtTexto.setText(txtTexto.getText() + linea);
                    }
                    fraw.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Altos Rosado"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("altosrosado")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Altos Tinto"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("altostinto")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Blanco"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("florblanco")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Rosado"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("florrosado")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Flor del Paraiso Tinto"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("flortinto")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Gredas Viejas"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("gredastinto")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;
            case ("Kylix Blanco"):
                try {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput("Kylixblanco")));
                    String texto = fin.readLine();
                    fin.close();
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde memoria interna");
                }

                break;

        /*btnsaac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saac = new Intent(ActivitySaac.this, MainActivity.class);
                startActivity(saac);
            }
        });*/
        }
    }
}
